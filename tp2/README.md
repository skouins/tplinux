# TP2 : Serveur Web
# Sommaire

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
    * [SELinux](#selinux)
    * [Réseau](#réseau)
* [I. Base de données](#i-base-de-données)
* [II. Serveur Web](#ii-serveur-web)
* [III. Reverse Proxy](#iii-reverse-proxy)
* [IV. Un peu de sécu](#iv-un-peu-de-sécu)
    * [1. fail2ban](#1-fail2ban)
    * [2. HTTPS](#2-https)
        * [Présentation](#présentation)
        * [A. Génération du certificat et de la clé privée associée](#a-génération-du-certificat-et-de-la-clé-privée-associée)
        * [B. Configurer le reverse proxy](#b-configurer-le-reverse-proxy)
    * [3. Monitoring](#3-monitoring)
        * [Présentation](#présentation-1)
        * [A. Installation de Netdata](#a-installation-de-netdata)
        * [B. Alerting](#b-alerting)
        * [C. Bonus](#c-bonus)
    * [4. Interface Web](#4-interface-web)

<!-- vim-markdown-toc -->


# I. Base de données

> A faire uniquement sur `db.tp2.cesi`.

Installer le paquet `mariadb-server`, puis démarrer le service associé.
```
sudo yum install mariadb-server

sudo systemctl enable mariadb

sudo systemctl start mariadb
```
Il faudra créer un utilisateur dans l'instance de base de données, ainsi qu'une base dédiée. Référez-vous à des docs/articles que vous pourrez trouver sur internet concernant la préparation d'une base de données pour une utilisation avec Wordpress. Les étapes :
* se connecter à la base
```
sudo mysql -u root -p
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
* créer une base
```
CREATE DATABASE dblab
Query OK, 1 row affected (0.00 sec)
```
* créer un utilisateur
```
CREATE USER 'dbuser2'@'%' identified by 'OKjuih!';
Query OK, 1 row affected (0.00 sec)
```
* attribuer les droits sur la base de données à l'utilisateur
```
GRANT ALL PRIVILEGES on dblab.* TO 'dbuser'@'localhost' identified by 'OKjuih!';
Query OK, 0 rows affected (0.00 sec)
GRANT ALL PRIVILEGES on dblab.* TO 'dbuser'@'db' identified by 'OKjuih!';
Query OK, 0 rows affected (0.00 sec)
```
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base
```
sudo firewall-cmd --add-port=3306/tcp --permanent
success
sudo firewall-cmd --reload
success
sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh dhcpv6-client
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
Pour connaître le port utilisé par la base de données, on peut uiliser la commande `ss`. Une fois la base de données lancée, on peut par exemple la commande suivante pour déterminer le port utilisé par la base :
```bash
[db@db ~]$ ss -alnpt
State       Recv-Q Send-Q                                                            Local Address:Port                                                                           Peer Address:Port
LISTEN      0      50                                                                            *:3306                                                                                      *:*
LISTEN      0      128                                                                           *:22                                                                                        *:*
LISTEN      0      100                                                                   127.0.0.1:25                                                                                        *:*
LISTEN      0      128                                                                          :::22                                                                                       :::*
LISTEN      0      100
```

Pour tester votre base de données :
```bash
[db@db ~]$  mysql -u dbuser2 -p -h 10.99.99.12 -P 3306 dblab
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 7
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
MariaDB [dblab]>
```
```
# 
MariaDB [dblab]> show databases;
+-----------------------+
| Tables_in_dblab       |
+-----------------------+
| wp_commentmeta        |
| wp_comments           |
| wp_links              |
| wp_options            |
| wp_postmeta           |
| wp_posts              |
| wp_term_relationships |
| wp_term_taxonomy      |
| wp_termmeta           |
| wp_terms              |
| wp_usermeta           |
| wp_users              |
+-----------------------+
12 rows in set (0.00 sec)
```

# II. Serveur Web

> A faire uniquement `web.tp2.cesi`

Installer un serveur Apache (paquet `httpd` dans CentOS).

Télécharger Wordpress : https://wordpress.org/latest.tar.gz

Idem ici, référez-vous à un des milliers d'article/doc/tuto que vous pourrez trouver. Faites vos recherches en anglais et précisez l'OS qu'on utilise.

Les étapes :
* installer le serveur Web (`httpd`) et le langage PHP (pour installer PHP, voir les instructions plus bas
```
sudo yum update httpd

sudo yum install httpd

sudo firewall-cmd --add-port=443/tcp --permanent
success

sudo firewall-cmd --add-port=80/tcp --permanent
success

sudo systemctl start httpd

sudo systemctl status httpd
 httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since jeu. 2021-01-07 13:12:47 CET; 46s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 12280 (httpd)
   Status: "Total requests: 0; Current requests/sec: 0; Current traffic:   0 B/sec"
   CGroup: /system.slice/httpd.service
           ├─12280 /usr/sbin/httpd -DFOREGROUND
           ├─12281 /usr/sbin/httpd -DFOREGROUND
           ├─12282 /usr/sbin/httpd -DFOREGROUND
           ├─12283 /usr/sbin/httpd -DFOREGROUND
           ├─12284 /usr/sbin/httpd -DFOREGROUND
           └─12285 /usr/sbin/httpd -DFOREGROUND

janv. 07 13:12:47 web systemd[1]: Starting The Apache HTTP Server...
janv. 07 13:12:47 web httpd[12280]: AH00558: httpd: Could not reliably de...ge
janv. 07 13:12:47 web systemd[1]: Started The Apache HTTP Server.
Hint: Some lines were ellipsized, use -l to show in full.
```
* télécharger wordpress
```
#curl -SLO https://wordpress.org/latest.tar.gz
```
* extraire l'archive wordpress dans un dossier qui pourra être servi par Apache
```
tar xzvf latest.tar.gz

sudo rsync -avP ~/wordpress/ /var/www/html/

mkdir /var/www/html/wp-content/uploads
```
* renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données (dans son fichier de configuration)
```
cp wp-config-sample.php wp-config.php
vi wp-config.php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dblab' );

/** MySQL database username */
define( 'DB_USER', 'dbuser2' );

/** MySQL database password */
define( 'DB_PASSWORD', 'OKjuih!' );

/** MySQL hostname */
define( 'DB_HOST', '10.99.99.12' );
```
* configurer Apache pour qu'il serve le dossier où se trouve Wordpress
```
sudo chown -R apache:apache /var/www/html/*
```
* lancer le serveur de base de donnée, puis le serveur web

* ouvrir le port firewall sur le serveur web

* accéder à l'interface de Wordpress afin de valider la bonne installation de la solution

Installation de PHP : la version de PHP disponible dans les dépôts officiels de CentOS7 n'est pas compatible avec la dernière version de Wrodpress disponible. Pour installer un version de PHP compatible :
```bash
# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
```

# III. Reverse Proxy

> A faire uniquement `rp.tp2.cesi`.

Installer un serveur NGINX (paquet `epel-release` puis `nginx` sur CentOS).
```
sudo yum install epel-release
sudo yum install nginx
sudo systemctl start nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2021-01-08 09:01:38 CET; 45s ago
 Main PID: 4156 (nginx)
   CGroup: /system.slice/nginx.service
           ├─4156 nginx: master process /usr/sbin/nginx
           └─4157 nginx: worker process

janv. 08 09:01:38 rp systemd[1]: Starting The nginx HTTP and reverse proxy server...
janv. 08 09:01:38 rp nginx[4150]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
janv. 08 09:01:38 rp nginx[4150]: nginx: configuration file /etc/nginx/nginx.conf test is successful
janv. 08 09:01:38 rp systemd[1]: Failed to read PID from file /run/nginx.pid: Invalid argument
janv. 08 09:01:38 rp systemd[1]: Started The nginx HTTP and reverse proxy server.

sudo systemctl enable nginx
Created symlink from /etc/systemd/system/multi-user.target.wants/nginx.service to /usr/lib/systemd/system/nginx.service.
```
Configurer NGINX pour qu'il puisse renvoyer vers le serveur web lorsqu'on l'interroge sur le port 80. Une configuration du fichier `/etc/nginx/nginx.conf` extrêmement minimale peut ressembler à :
```
events {}

http {
    server {
        listen       80;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://10.99.99.11:80;
        }
    }
}

sudo systemctl restart nginx

```
 

###Tester qu'on accède bien au Wordpress en passant par l'IP du reverse proxy. 

on accède bien au site par le proxy mais on a une version du site wordpress sans CSS



## 1. fail2ban

* il lit en temps réel le fichier de log du démon SSH `sshd`
* `sshd` écrit chaque tentative de connexion dans le fichier
* si un certain nombre d'échecs de connexions sont réalisés dans un certain laps de temps, fail2ban devra le détecter
* fail2ban utilisera alors le pare-feu pour bloquer l'IP qui a essayé de se connecter
```
sudo yum install fail2ban
sudo systemctl start fail2ban
sudo systemctl enable fail2ban

[sshd]
enabled = true
port = ssh
action = iptables-multiport
logpath = /var/log/audit/audit.log
findtime = 600
maxretry = 5
bantime = 600
```
```
[db@rp ~]$ systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since dim. 2021-01-10 16:47:40 CET; 14s ago
     Docs: man:fail2ban(1)
  Process: 30468 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 30469 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─30469 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start

janv. 10 16:47:40 rp systemd[1]: Starting Fail2Ban Service...
janv. 10 16:47:40 rp systemd[1]: Started Fail2Ban Service.
janv. 10 16:47:41 rp fail2ban-server[30469]: Server ready
```

## 2. HTTPS


### A. Génération du certificat et de la clé privée associée

> A faire sur la machine frontale Web : le reverse proxy.

Commande pour générer une clé et un certificat auto-signé :
```bash
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.tp2.cesi.key -out web.tp2.cesi.crt
```

**NB :** lorsque vous taperez la commande de génération de certificat, un prompt vous demandera des infos qui seront inscrites dans le certificat. Vous pouvez mettre ce que vous voulez **SAUF** pour l'étape où on vous demande :
```bash
Common Name (e.g. server FQDN or YOUR name) []:
```
```
Generating a 2048 bit RSA private key
........+++
........................................................+++
writing new private key to 'web.tp2.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:bordeaux
Locality Name (eg, city) [Default City]:bdx
Organization Name (eg, company) [Default Company Ltd]:me
Organizational Unit Name (eg, section) []:meme
Common Name (eg, your name or your server's hostname) []:web.tp2.cesi
Email Address []:
```


Le certificat et la clé ont été générés dans le répertoire courant. Vous pouvez les déplacez où vous le souhaitez ; il existe cependant un path réservé à ça dans CentOS :
* `/etc/pki/tls/certs/` pour les certificats
```
sudo mv web.tp2.cesi.crt /etc/pki/tls/certs
```
* `/etc/pki/tls/private/` pour les clés
```
sudo mv web.tp2.cesi.key /etc/pki/tls/private
```

### B. Configurer le reverse proxy

> A faire sur la machine frontale Web : le reverse proxy.

Configurer le reverse proxy pour écouter sur le port 443 (TCP). Le port 443 doit mettre en place un chiffrement TLS avec la clé et le certificat qui viennent d'être générés. Il doit toujours rediriger vers l'application Web Wordpress.

Si le port 80 est atteint, le trafic doit être redirigé automatiquement vers le port 443 (ça se fait dans la configuration de NGINX).

> Gardez en tête la commande `sudo ss -alnpt` pour avoir une idée des ports TCP en écoute sur votre machine. Vous pouvez vous assurer que votre serveur tourne bien sur le port `80/tcp` et `443/tcp` de cette façon. Pour tester la redirection automatique de port 80 vers 443, il faudra vous connecter au site comme un client normal, avec un navigateur.

N'oubliez pas d'ajouter au fichier `hosts` du CLIENT que `web.tp2.cesi` doit pointer vers l'adresse IP du reverse proxy. Si vous testez avec votre machine hôte et votre navigateur, c'est au fichier `hosts` de VOTRE PC qu'il faut ajouter l'IP et le nom `web.tp2.cesi`.
```
  events {}

http {
    server {
        listen 80 default_server;

        server_name _;

        return 301 https://web.tp2.cesi$request_uri;
    }
    server {

          listen 443 ssl default_server;

          server_name web.tp2.cesi;
          # ssl  files
          ssl on;
          ssl_certificate     /etc/pki/tls/certs/web.tp2.cesi.crt;
          ssl_certificate_key /etc/pki/tls/private/web.tp2.cesi.key;
          keepalive_timeout   60;
          location / {
            proxy_pass   http://10.99.99.11:80;
          }
    }
    server {

        listen 443 ssl;
        server_name web.tp2.cesi;

    }
}
```
## 3. Monitoring


### A. Installation de Netdata

Installer Netdata sur les 3 machines et tester qu'il fonctionne bien en accédant à l'interface Web. Il faudra ouvrir le port firewall `19999/tcp` pour accéder à l'interface de Netdata.
```
[db@rp ~]$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
 --- Found existing install of Netdata under: / ---
 --- Attempting to update existing install instead of creating a new one ---
[/home/db]$ sudo //usr/libexec/netdata/netdata-updater.sh --not-running-from-cron [sudo] Mot de passe de db : 
Checking if a newer version of the updater script is available.
dim. janv. 10 17:20:14 CET 2021 : INFO:  Running on a terminal - (this script also supports running headless from crontab)
//usr/libexec/netdata/netdata-updater.sh: ligne101: _cannot_find_tmpdir : commande introuvable
dim. janv. 10 17:20:14 CET 2021 : INFO:  Current Version: 00102800000093
dim. janv. 10 17:20:14 CET 2021 : INFO:  Latest Version: 00102800000093
dim. janv. 10 17:20:14 CET 2021 : INFO:  Newest version (current=00102800000093 >= latest=00102800000093) is already installed
 OK

 --- Updated existing install at /usr/sbin/netdata ---
```
### B. Alerting

Définir une configuration qui permet de recevoir une [alerte Discord](https://learn.netdata.cloud/docs/agent/health/notifications/discord) si le disque OU la RAM sont remplis à + de 75%.
```
cd /etc/netdata/
sudo ./edit-config health.d/example.conf
```  
### C. Bonus

En bonus : ajouter une configuration au Reverse Proxy pour qu'il permette d'accéder aux trois interfaces de Netdata, avec du chiffrement.

On peut aussi affiner la configuration réseau du parc pour empêcher que l'application Web ou les interfaces de Netdata soient disponibles sans passer par le reverse proxy. Plusieurs façons de faire, par ex :

**Faire ça niveau 1**
* on crée un non pas un, mais deux réseaux locaux
* l'un est utilisé pour la communication entre nos serveurs + l'administration *via* SSH
* un deuxième est utilisé entre les clients et les serveurs frontaux (en l'occurrence, juste notre reverse proxy)

> Ca arrive même d'en voir un troisième réservé à l'admin SSH exclusivement, un autre pour les flux de backup, etc.

**Faire ça niveau 3**
* configurer le pare-feu des serveurs Web et base de données pour qu'ils refusent toutes connexions non attendues
* on blacklist tout
* le serveur web whitelist la base de données et le reverse proxy
* la base de de donnée whitelist que le serveur Web
* n'oubliez pas qu'il y a aussi votre accès SSH

Liste non-exhautive des solutions. Plus c'est fait tôt (niveau 1), plus c'est radical, et moins la surface d'attaque est grande, mais plus c'est coûteux à mettre en oeuvre. Plus c'est fait tard (niveau 3) plus c'est compliqué à maintenir dans le temps (en l'occurrence : il faut les maintenir les règles firewall).

On peut aussi cumuler les deux mesures sans problèmes pour minimiser la surface d'attaque de nos serveurs.

## 4. Interface Web

Un outil qu'il est possible d'utiliser pour contrôler certains aspects du serveur est Cockpit.

Son installation est très simple :
```bash
$ sudo yum install -y epel-release
$ sudo yum install -y cockpit
```
testé l'install et reussi 