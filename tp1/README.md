# TP1 : Configuration système



# Sommaire

<!-- vim-markdown-toc GitLab -->

* [0. Préliminaires](#0-préliminaires)
* [I. Utilisateurs](#i-utilisateurs)
    * [1. Création et configuration](#1-création-et-configuration)
    * [2. SSH](#2-ssh)
* [II. Configuration réseau](#ii-configuration-réseau)
    * [1. Nom de domaine](#1-nom-de-domaine)
    * [2. Serveur DNS](#2-serveur-dns)
* [III. Partitionnement](#iii-partitionnement)
    * [1. Préparation de la VM](#1-préparation-de-la-vm)
    * [2. Partitionnement](#2-partitionnement)
* [IV. Gestion de services](#iv-gestion-de-services)
    * [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
    * [2. Création de service](#2-création-de-service)
        * [A. Unité simpliste](#a-unité-simpliste)
        * [B. Modification de l'unité](#b-modification-de-lunité)

<!-- vim-markdown-toc -->




# I. Utilisateurs

## 1. Création et configuration

Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :
* le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
* le shell de l'utilisateur est `/bin/bash`


Créer un nouveau groupe `admins` qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

Pour permettre à ce groupe d'accéder aux droits `root` :
* il faut modifier le fichier `/etc/sudoers`
* on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
* la commande `visudo` permet d'éditer le fichier, avec un check de syntaxe avant fermeture
* ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe `wheel`)

Ajouter votre utilisateur à ce groupe `admins`.

---

1. Utilisateur créé et configuré
```
sudo useradd johann -m -p Okjuih! -s /bin/bash
[djo@djopc home]$ ls
djo  jj  johann  web

[djo@djopc home]$ cat /etc/passwd
...
...
johann:x:1003:1003::/home/johann:/bin/bash

```
2. Groupe `admins` créé
```
[djo@djopc home]$ sudo groupadd admins

[djo@djopc home]$ cat /etc/group
...
...
admins:x:1004:
```
3. Groupe `admins` ajouté au fichier `/etc/sudoers`
```
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
%admins  ALL=(ALL)       ALL
```


4. Ajout de l'utilisateur au groupe `admins`
```
[djo@djopc home]$ sudo usermod  johann -aG admins

[johann@djopc ~]$ groups
johann admins
```
## 2. SSH

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine. 

Il existe une [section dédiée dans la partie cours](../../cours/ssh_keys.md).

Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.
```
PS C:\Users\Jo> ssh djo@10.88.88.11
Last login: Thu Jan  7 18:59:54 2021 from 10.88.88.1

[djo@djopc ~]$
```

# II. Configuration réseau

## 1. Nom de domaine

Définir un nom de domaine à la machine
```
[djo@djopc ~]$ hostname
djopc
```
```
pour modifier le nom d'hote de facon permanente
sudo vi /etc/hostname
```

## 2. Serveur DNS

Définir l'utilisation de `1.1.1.1` (ou autre adresse personnalisée) comme serveur DNS de la machine.

# Contenu du fichier

```
$ sudo cat /etc/resolv.conf
nameserver 1.1.1.1

[djo@djopc ~]$ curl google.com
<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>301 Moved</TITLE></HEAD><BODY>
<H1>301 Moved</H1>
The document has moved
<A HREF="http://www.google.com/">here</A>.
</BODY></HTML>
```

# III. Partitionnement

## 1. Préparation de la VM

* Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.
``` 
$ sudo pvcreate /dev/sdb
$ sudo pvcreate /dev/sdc
Physical volume "/dev/sdb" successfully created.
Physical volume "/dev/sdc" successfully created.
 ``` 
## 2. Partitionnement

Utilisez LVM pour :
* agréger les deux disques en un seul *volume group*
``` 
sudo vgcreate data2 /dev/sdb /dev/sdc
Volume group "data2" successfully created

 PV         VG     Fmt  Attr PSize  PFree
  /dev/sda2  centos lvm2 a--  <7,00g    0
  /dev/sdb   data2  lvm2 a--  <3,00g    0
  /dev/sdc   data2  lvm2 a--  <3,00g    0
```
* créer 3 *logical volumes* de 2 Go chacun
```  
  LV     VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root   centos -wi-ao----  <6,20g
  swap   centos -wi-ao---- 820,00m
  Free   data2  -wi-a-----   1,99g
  thirst data2  -wi-a-----   2,00g
  too    data2  -wi-a-----   2,00g
``` 

* formater ces partitions en `ext4`
```
sudo mkfs -t ext4 /dev/data2/thirst
sudo mkfs -t ext4 /dev/data2/Free
sudo mkfs -t ext4 /dev/data2/too

Exemple:
mke2fs 1.42.9 (28-Dec-2013)
Étiquette de système de fichiers=
Type de système d'exploitation : Linux
Taille de bloc=4096 (log=2)
Taille de fragment=4096 (log=2)
« Stride » = 0 blocs, « Stripe width » = 0 blocs
131072 i-noeuds, 524288 blocs
26214 blocs (5.00%) réservés pour le super utilisateur
Premier bloc de données=0
Nombre maximum de blocs du système de fichiers=536870912
16 groupes de blocs
32768 blocs par groupe, 32768 fragments par groupe
8192 i-noeuds par groupe
Superblocs de secours stockés sur les blocs :
        32768, 98304, 163840, 229376, 294912

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (16384 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété

```
 
* monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1`, `/mnt/part2` et `/mnt/part3`.
```
sudo mount /dev/data2/thirst /mnt/thirst
sudo mount /dev/data2/too /mnt/too
sudo mount /dev/data2/Free /mnt/free

Sys. de fichiers         Taille Utilisé Dispo Uti% Monté sur
devtmpfs                   908M       0  908M   0% /dev
tmpfs                      919M       0  919M   0% /dev/shm
tmpfs                      919M    8,6M  911M   1% /run
tmpfs                      919M       0  919M   0% /sys/fs/cgroup
/dev/mapper/centos-root    6,2G    1,5G  4,8G  24% /
/dev/sda1                 1014M    189M  826M  19% /boot
tmpfs                      184M       0  184M   0% /run/user/1000
/dev/mapper/data2-thirst   2,0G    6,0M  1,8G   1% /mnt/thirst
/dev/mapper/data2-too      2,0G    6,0M  1,8G   1% /mnt/too
/dev/mapper/data2-Free     2,0G    6,0M  1,9G   1% /mnt/free
```

Grâce au fichier `/etc/fstab`, faites en sorte que cette partition soit montée automatiquement au démarrage du système.
```
# /etc/fstab
# Created by anaconda on Tue Jan  5 21:00:17 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=cd6ccc92-6663-4864-bc18-765116a808df /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/data2/thirst /mnt/thirst                   ext4    defaults        0 0
/dev/data2/too /mnt/too                         ext4    defaults        0 0
/dev/data2/Free /mnt/free                       ext4    defaults        0 0
```



# IV. Gestion de services

Au sein des systèmes GNU/Linux les plus utilisés, c'est *systemd* qui est utilisé comme gestionnaire de services (entre autres).

Pour manipuler les services entretenus par *systemd*, on utilise la commande `systemctl`.

On peut lister les unités `systemd` actives de la machine `systemctl list-units -t service`.

> Beaucoup de commandes `systemd` qui écrivent des choses en sortie standard sont automatiquement munie d'un pager (pipé dans `less`). On peut ajouter l'option `--no-pager` pour se débarasser de ce comportement

Pour obtenir plus de détails sur une unitée donnée
* `systemctl is-active <UNIT>`
  * détermine si l'unité est actuellement en cours de fonctionnement
* `systemctl is-enabled <UNIT>`
  * détermine si l'unité est activée ("activée" = démarre au boot)
* `systemctl status <UNIT>`
  * affiche l'état complet d'une unité donnée
  * comme le path où elle est définie, si elle a été enable, tous les processus liés, etc.
* `systemctl cat <UNIT>`
  * affiche les fichiers qui définissent l'unité

## 1. Interaction avec un service existant

Parmi les services système déjà installés sur CentOS, il existe `firewalld`. Cet utilitaire est l'outil de firewalling de CentOS.

Assurez-vous que :
* l'unité est démarrée
```
 firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since jeu. 2021-01-07 18:58:43 CET; 3h 26min ago
     Docs: man:firewalld(1)
 Main PID: 761 (firewalld)
   CGroup: /system.slice/firewalld.service
           └─761 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid

janv. 07 18:58:42 djopc systemd[1]: Starting firewalld - dynamic firewall daemon...
janv. 07 18:58:43 djopc systemd[1]: Started firewalld - dynamic firewall daemon.
janv. 07 18:58:43 djopc firewalld[761]: WARNING: AllowZoneDrifting is enabled. This is considered an insecure configuration option. It will be removed in a future release. Please consider disabling it now.

```
* l'unitée est activée (elle se lance automatiquement au démarrage)
```
sudo systemctl enable firewalld
```
## 2. Création de service

### A. Unité simpliste

Créer un fichier qui définit une unité de service `web.service` dans le répertoire `/etc/systemd/system`.

Déposer le contenu suivant :
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target
```

Le but de cette unité est de lancer un serveur web sur le port 8888 de la machine. **N'oubliez pas d'[ouvrir ce port](../../cours/centos_network.md#interagir-avec-le-firewall).**
```
sudo firewall-cmd --add-port=8888/tcp --permanent

State      Recv-Q Send-Q Local Address:Port               Peer Address:Port
LISTEN     0      128          *:22                       *:*                   users:(("sshd",pid=1135,fd=3))
LISTEN     0      5            *:8888                     *:*                   users:(("python2",pid=2106,fd=3))
LISTEN     0      100    127.0.0.1:25                       *:*                   users:(("master",pid=1459,fd=13))
LISTEN     0      128       [::]:22                    [::]:*                   users:(("sshd",pid=1135,fd=4))
LISTEN     0      100      [::1]:25                    [::]:*                   users:(("master",pid=1459,fd=14))
```
Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :
```bash
$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité :
```bash
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2021-01-07 18:58:42 CET; 3h 35min ago
 Main PID: 742 (python2)
   CGroup: /system.slice/web.service
           └─742 /bin/python2 -m SimpleHTTPServer 8888
           
$ sudo systemctl start web
$ sudo systemctl enable web
```

Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande `curl` sur l'IP de la VM, port 8888.

### B. Modification de l'unité

Créer un utilisateur `web`.

Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses :
* `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
* `WorkingDirectory=` afin de lancer le serveur depuis un dossier spécifique, choisissez un dossier que vous avez créé dans `/srv`
* ces deux clauses sont à positionner dans la section `[Service]` de votre unité

Placer un fichier de votre choix dans le dossier créé dans `/srv` et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur `web`.

---

Pour tester le bon fonctionnement :
* depuis la VM
  * `sudo ss -alnpt` pour voir le port ouvert et le service qui écoute derrière
```

State       Recv-Q Send-Q                                                            Local Address:Port                                                                           Peer Address:Port
LISTEN      0      128                                                                           *:22                                                                                        *:*                   users:(("sshd",pid=1139,fd=3))
LISTEN      0      5                                                                             *:8888                                                                                      *:*                   users:(("python2",pid=742,fd=3))
LISTEN      0      100                                                                   127.0.0.1:25                                                                                        *:*                   users:(("master",pid=1368,fd=13))
LISTEN      0      128                                                                        [::]:22                                                                                     [::]:*                   users:(("sshd",pid=1139,fd=4))
LISTEN      0      100                                                                       [::1]:25                                                                                     [::]:*                   users:(("master",pid=1368,fd=14))

```
  * `curl localhost:8888` pour envoyer une requête HTTP au service
```
[djo@djopc ~]$ curl localhost:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="lab">lab</a>
</ul>
<hr>
</body>
</html>
```
* depuis l'hôte
  * dans un navigateur web : `http://<IP_VM>:8888`